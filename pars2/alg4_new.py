#!/usr/bin/env python
# -*- coding: utf-8 -*-



import time
import os
import pandas
from datetime import datetime

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException


doc_name_full = "22-05-2020_all_data.csv"
doc_phone = "22-05-2020_phone.csv"


def get_driver_mobile():
    mobile_emulation = {
        "deviceMetrics": {"width": 360, "height": 640, "pixelRatio": 3.0},
        "userAgent": "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
    }

    options = Options()
    options.add_experimental_option("mobileEmulation", mobile_emulation)
    caps = DesiredCapabilities().CHROME
    driver = webdriver.Chrome(
        executable_path='%s/chromedriver'%(os.getcwd()),
        desired_capabilities=caps,
        chrome_options = options,
    )
    time.sleep(2)
    return driver


# создаем шапку в файле для сохранения всех данных
def create_csv_header():
    f = open(doc_name_full,'a+')
    f.write('"город";"рубрика";"категория";"подкатегория";"ссылка";"название";"тип предприятия";"номер телефона";"ссылки";"email";"адрес";"СФЕРА ДЕЯТЕЛЬНОСТИ";"ТИП ПРОЕКТА";"Источник";\n')
    f.close()

    f = open(doc_phone, 'a+')
    f.write('"город";"рубрика";"категория";"подкатегория";"ссылка";"название";"тип предприятия";"номер телефона";"ссылки";"email";"адрес";"СФЕРА ДЕЯТЕЛЬНОСТИ";"ТИП ПРОЕКТА";"Источник";\n')
    f.close()


# получаем список из url которые нужно пропарсить
def get_urls(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    lal = soup.find_all("article", {'class': 'minicardPlain__container'})

    res_list = []
    for i in range(len(lal)):
        res_1 = driver.find_elements_by_class_name("minicardPlain__title")[i]
        res = res_1.find_elements_by_tag_name("a")[0].get_attribute('href')
        res_list.append(res)

    time.sleep(1)
    return res_list


# основаная функция для получения списка из url - клик, прокрутка, получение url
def get_list_of_urls(driver):

    # дабы была возможность начать скролить нужно нажать на блок
    driver.execute_script("document.getElementsByClassName('searchResults__content')[0].click();")
    time.sleep(1)

    try:
        while driver.find_element_by_xpath("//div[@class='preloader__container']"):
            driver.execute_script(""" if (document.getElementsByClassName('preloader__container')[0] != null) {
                                            document.getElementsByClassName('preloader__container')[0].scrollIntoView({block: 'center', behavior: 'smooth'})
                                        }""")
            time.sleep(1)
    except NoSuchElementException:
        time.sleep(2)
        result = get_urls(driver)
        return result


def save_data_in_file(filename, city, rubr, category, url, pr_title, pr_sub_title, pr_phone_res, pr_urls_res, pr_email_res, pr_address_res):
    f = open(filename, 'a+')
    f.write('"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"ДРУГАЯ";"ДРУГОЙ";"SIP телефония: продажи";\n' % (
                                                city.encode('utf-8'),
                                                rubr,  # рубрика
                                                category["category"].replace(";", " ") if category["category"] else rubr,  # категория
                                                category["sub-category"].replace(";", " ") if category["sub-category"] else rubr,  # под-категория
                                                url.encode('utf-8'),
                                                pr_title.encode('utf-8') if pr_title else '',
                                                pr_sub_title.encode('utf-8') if pr_sub_title else '',
                                                pr_phone_res.encode('utf-8') if pr_phone_res else '',
                                                pr_urls_res if pr_urls_res else '',
                                                pr_email_res if pr_email_res else '',
                                                pr_address_res.encode('utf-8') if pr_address_res else ''
                                                ))
    f.close()


# парсим данные
def get_data(driver, url, city, category):
    print(url)
    driver.get(url)
    time.sleep(2)

    pr_title = driver.find_element_by_xpath("//h1[@class='firmCardHeader__title']").text
    pr_title = pr_title.replace(";", " ")

    try:
        pr_sub_title = driver.find_element_by_xpath("//h2[@class='firmCardHeader__extension']").text
        pr_sub_title = " - ".join(pr_sub_title.split(","))
        pr_sub_title = pr_sub_title.replace(";", "")
    except NoSuchElementException:
        pr_sub_title = ""

    pr_phone = []
    pr_urls = []
    email_urls = []
    pr_phone_res = ""
    pr_urls_res = ""
    pr_email_res = ""
    # contacts = driver.find_element_by_xpath("//div[@class='contacts__list']/div/div/a")
    # contacts = driver.find_elements_by_class_name("contacts__contact").
    if driver.find_elements_by_class_name("contacts__list"):
        contacts = driver.find_elements_by_class_name("contacts__list")[0].find_elements_by_tag_name("a")
        for contact in contacts:
            if "tel" in contact.get_attribute('href'):
                pr_phone.append(contact.get_attribute('href').encode('utf-8'))
                pr_phone_res = "   ".join(i for i in pr_phone)
                pr_phone_res = pr_phone_res.replace("tel:+", ",")
            elif "@" in contact.get_attribute('href'):
                email_urls.append(contact.text.encode('utf-8'))
                pr_email_res = "   ".join(i for i in email_urls)
            elif "facebook" not in contact.get_attribute('href') or "insta" not in contact.get_attribute('href'):
                pr_urls.append(contact.text.encode('utf-8'))
                pr_urls.append(contact.get_attribute('href').encode('utf-8'))
                pr_urls_res = "   ".join(i for i in pr_urls)

    pr_address = driver.find_element_by_xpath("//address[@class='address__container']").text
    pr_address_res = " ".join(i for i in pr_address.split()).replace(",", "").replace(";", "")

    print(pr_title) if pr_title else ''
    print(pr_sub_title) if pr_sub_title else ''
    print(pr_phone_res) if pr_phone_res else ''
    print(pr_address_res) if pr_address_res else ''
    print(pr_email_res) if pr_email_res else ''
    print("pr_phone_res  ", pr_phone_res) if pr_phone_res else ''

    rubr = category["rubric"].decode('utf8').encode('utf-8').replace("%20", " ")

    # save data - all data
    save_data_in_file(doc_name_full, city, rubr, category, url, pr_title, pr_sub_title, pr_phone_res, pr_urls_res, pr_email_res, pr_address_res)
    print("Сохраняем все данные!!!")

    if pr_phone_res:
        save_data_in_file(doc_phone, city, rubr, category, url, pr_title, pr_sub_title, pr_phone_res, pr_urls_res,
                          pr_email_res, pr_address_res)
        print("Сохраняем данные c номером телефона!!!")
    print('--------------------------------------')

    return driver



def main():
    city = "kharkov"
    # data = [
    #     {"pars": "Шиномонтаж", "rubric": "Шиномонтаж", "category": "", "sub-category":""},   # 334
    #     {"pars": "Автосервис", "rubric": "Автосервис", "category": "", "sub-category":""},   # 425
    #     {"pars": "Аптеки", "rubric": "Аптеки", "category": "", "sub-category":""},   # 683
    #     {"pars": "Доставка%20продуктов", "rubric": "Доставка продуктов", "category": "", "sub-category":""},  # 50
    #     {"pars": "Доставка%20еды", "rubric": "Доставка еды", "category": "", "sub-category":""},  # 290
    # ]

    # add header to csv file
    # create_csv_header()

    df = pandas.read_csv('cat_parse.csv', index_col=False)
    data = df.T.to_dict().values()

    for category in data[818:]:   # 687 was
        delivery_product_m = "https://m.2gis.ua/%s/search/%s" % (city, category["pars"])
        # print(delivery_product_m)

        # start parsing
        driver = get_driver_mobile()
        driver.get(delivery_product_m)

        list_of_urls = get_list_of_urls(driver)
        print("All data - %s"%(len(list_of_urls)))
        for nmb, url in enumerate(list_of_urls):
            print("Time - %s . Parent page - %s"%(datetime.now(), delivery_product_m))
            driver = get_data(driver, url, city, category)

    #####   FOR TESTING ONLY
    # category = data[52]
    # delivery_product_m = "https://m.2gis.ua/%s/search/%s" % (city, category["pars"])
    #
    # # start parsing
    # driver = get_driver_mobile()
    # driver.get(delivery_product_m)
    #
    # list_of_urls = get_list_of_urls(driver)
    # print("All data - %s"%(len(list_of_urls)))
    # for nmb, url in enumerate(list_of_urls):
    #     print("Time - %s . Parent page - %s"%(datetime.now(), delivery_product_m))
    #     driver = get_data(driver, url, city, category)


if __name__ == '__main__':
    main()

