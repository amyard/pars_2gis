#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import os

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


delivery_product = "https://2gis.ua/kharkov/search/%D0%94%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D0%BE%D0%B2"


# def get_driver(url):
#     options = Options()
#     caps = DesiredCapabilities().CHROME
#     driver = webdriver.Chrome(
#         executable_path='%s/chromedriver'%(os.getcwd()),
#         desired_capabilities=caps,
#         chrome_options = options,
#     )
#     driver.set_window_size(1920, 800)
#     driver.maximize_window()
#     driver.get(url)
#     time.sleep(2)
#     return driver
#
#
#
# # iterate throught list of data
# driver = get_driver(delivery_product)
# time.sleep(2)
#
# soup = BeautifulSoup(driver.page_source, 'html.parser')
# lal = soup.find_all("div", {'class': '_y3rccd'})
#
# for i in range(len(lal)):
#     driver.find_element_by_xpath(f"//div[@class='_awwm2v']/div[{i+1}]").click()
#     time.sleep(2)




#############################################################################

import time
import os

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


delivery_product = "https://2gis.ua/kharkov/search/%D0%94%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D0%BE%D0%B2"


section_product = "_y3rccd"
section_product_item = "_awwm2v"
section_product_item_address = "_84s065h"
section_product_item_phone_1 = "_b0ke8"
section_product_item_phone_2 = "_84s065h"
section_product_item_street = "_er2xx9"
section_product_item_address_1 = "_13eh3hvq"
section_product_item_email = "_13ptbeu"



def get_driver(url):
    options = Options()
    caps = DesiredCapabilities().CHROME
    driver = webdriver.Chrome(
        executable_path='%s/chromedriver'%(os.getcwd()),
        desired_capabilities=caps,
        chrome_options = options,
    )
    driver.set_window_size(1920, 800)
    driver.maximize_window()
    driver.get(url)
    time.sleep(2)
    return driver



# iterate throught list of data
driver = get_driver(delivery_product)
time.sleep(2)

soup = BeautifulSoup(driver.page_source, 'html.parser')
lal = soup.find_all("div", {'class': section_product})

for i in range(len(lal)):
    driver.find_element_by_xpath(f"//div[@class='{section_product_item}']/div[{i+1}]").click()
    time.sleep(2)
    product_title = driver.find_elements_by_tag_name("h1")[0].text
    product_category = driver.find_element_by_xpath("//h1/following-sibling::div").text
    product_phone = driver.find_element_by_xpath(f"//div[@class='{section_product_item_phone_1}']/a[@class='{section_product_item_phone_2}']").text()
    pr_street = driver.find_element_by_xpath(f"//span[@class='{section_product_item_street}']").text
    pr_street = driver.find_element_by_xpath(f"//div[@class='{section_product_item_address}']").text

    print(product_title, product_category, product_phone)
    print("-------------------------------------------------------------------")




# driver.find_element_by_xpath(f"//div[@class='{section_product_item}']/div[2]").click()
# time.sleep(2)
# product_title = driver.find_elements_by_tag_name("h1")[0].text
# product_category = driver.find_element_by_xpath("//h1/following-sibling::div").text
# product_phone = driver.find_element_by_xpath(f"//div[@class='{section_product_item_phone_1}']/a[@class='{section_product_item_phone_2}']").get_attribute('href')
# pr_street = driver.find_element_by_xpath(f"//span[@class='{section_product_item_street}']").text
#
#
# pr_address = driver.find_element_by_xpath(f"//div[@class='{section_product_item_address_1}']").text
#
#
# pr_email = driver.find_element_by_xpath(f"//a[@class='{section_product_item_email}']").text
#
#
# print(pr_address)
# print(pr_email)

