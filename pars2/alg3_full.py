#!/usr/bin/env python
# -*- coding: utf-8 -*-



import time
import os

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


doc_name = "test.csv"


def get_driver_mobile():
    mobile_emulation = {
        "deviceMetrics": {"width": 360, "height": 640, "pixelRatio": 3.0},
        "userAgent": "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
    }

    options = Options()
    options.add_experimental_option("mobileEmulation", mobile_emulation)
    caps = DesiredCapabilities().CHROME
    driver = webdriver.Chrome(
        executable_path='%s/chromedriver'%(os.getcwd()),
        desired_capabilities=caps,
        chrome_options = options,
    )
    time.sleep(2)
    return driver


# создаем шапку в файле для сохранения всех данных
def create_csv_header():
    f = open(doc_name,'a+')
    f.write("город,рубрика,категория,подкатегория,ссылка,название,тип предприятия,номер телефона,ссылки,адрес,\n")
    f.close()


# скролит автоматичестки вниз дабы загрузились все url для парсинга
def scroll_down(driver):
    driver.execute_script(""" function scrollDown() {
                                    if (document.getElementsByClassName('preloader__container')[0] != null) {
                                        document.getElementsByClassName('preloader__container')[0].scrollIntoView({block: 'center', behavior: 'smooth'})
                                        setTimeout(function(){ scrollDown() }, 500)
                                    }
                                }
                                scrollDown();""")

    # TODO - время нужно просчитать , сколько было скроллов вниз на будущее
    time.sleep(10)
    return driver


# получаем список из url которые нужно пропарсить
def get_urls(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    lal = soup.find_all("article", {'class': 'minicardPlain__container'})

    res_list = []
    for i in range(len(lal)):
        res_1 = driver.find_elements_by_class_name("minicardPlain__title")[i]
        res = res_1.find_elements_by_tag_name("a")[0].get_attribute('href')
        res_list.append(res)

    time.sleep(1)
    return res_list


# основаная функция для получения списка из url - клик, прокрутка, получение url
def get_list_of_urls(driver):
    # дабы была возможность начать скролить нужно нажать на блок
    driver.execute_script("document.getElementsByClassName('searchResults__content')[0].click();")
    time.sleep(1)

    driver = scroll_down(driver)
    result = get_urls(driver)
    return result


# парсим данные
def get_data(driver, url, city, category):

    driver.get(url)
    time.sleep(2)

    pr_title = driver.find_element_by_xpath("//h1[@class='firmCardHeader__title']").text
    pr_sub_title = driver.find_element_by_xpath("//h2[@class='firmCardHeader__extension']").text

    pr_phone = []
    pr_urls = []
    pr_phone_res = ""
    pr_urls_res = ""
    contacts = driver.find_elements_by_class_name("contacts__contact")
    for contact in contacts:
        if "tel" in contact.get_attribute('href'):
            pr_phone.append(contact.get_attribute('href').decode().encode('utf-8'))
            pr_phone_res = "   ".join(i for i in pr_phone)
        elif "facebook" not in contact.get_attribute('href') or "insta" not in contact.get_attribute('href'):
            pr_urls.append(contact.text.decode().encode('utf-8'))
            pr_urls_res = "   ".join(i for i in pr_urls)

    pr_address = driver.find_element_by_xpath("//address[@class='address__container']").text
    pr_address_res = " ".join(i for i in pr_address.split()).replace(",", "")

    print(pr_title) if pr_title else ''
    print(pr_sub_title) if pr_sub_title else ''
    print(pr_phone_res) if pr_phone_res else ''
    print(pr_urls_res) if pr_urls_res else ''
    print(pr_address_res) if pr_address_res else ''

    # save data
    f = open(doc_name, 'a+')
    f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,\n"%(city.encode('utf-8'),
                                          category["rubric"].decode('utf8').encode('utf-8'),
                                          category["category"].decode('utf8').encode('utf-8'),
                                          category["sub_category"].decode('utf8').encode('utf-8'),
                                          url.encode('utf-8'),
                                          pr_title.encode('utf-8') if pr_title else '',
                                          pr_sub_title.encode('utf-8') if pr_sub_title else '',
                                          pr_phone_res.encode('utf-8') if pr_phone_res else '',
                                          pr_urls_res.encode('utf-8') if pr_urls_res else '',
                                          pr_address_res.encode('utf-8') if pr_address_res else ''
                                          ))
    f.close()
    print('--------------------------------------')

    return driver


def main():
    city = "kharkov"
    data = [
        {"pars": "Доставка%20продуктов", "rubric": "Доставка продуктов",  "category": "Доставка продуктов", "sub_category": "Доставка продуктов"},   # 50
        {"pars": "Доставка%20еды", "rubric": "Доставка еды",  "category": "Доставка еды", "sub_category": "Доставка еды"},   # 290
        {"pars": "Аптеки", "rubric": "Аптеки",  "category": "Аптеки", "sub_category": "Аптеки"},   # 683
        {"pars": "Шиномонтаж", "rubric": "Шиномонтаж",  "category": "Шиномонтаж", "sub_category": "Шиномонтаж"},   # 334
        {"pars": "Автосервис", "rubric": "Автосервис",  "category": "Автосервис", "sub_category": "Автосервис"},   # 425



        # {"pars": "", "rubric": "",  "category": "", "sub_category": ""},   # 50
    ]


    for category in data:
        delivery_product_m = "https://m.2gis.ua/%s/search/%s" % (city, category["pars"])
        print(delivery_product_m)

        # # add header to csv file
        # create_csv_header()
        #
        # # start parsing
        # driver = get_driver_mobile()
        # driver.get(delivery_product_m)
        #
        # list_of_urls = get_list_of_urls(driver)
        # for nmb, url in enumerate(list_of_urls):
        #     driver = get_data(driver, url, city, category)




if __name__ == '__main__':
    main()

