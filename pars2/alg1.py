#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import time
import re
import json
import os

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


url = "https://2gis.ua/kharkov/search/%D0%94%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D0%BE%D0%B2"


def get_driver(url):
    options = Options()
    caps = DesiredCapabilities().CHROME
    driver = webdriver.Chrome(
        executable_path='%s/chromedriver'%(os.getcwd()),
        desired_capabilities=caps,
        chrome_options = options,
    )
    driver.set_window_size(1920, 800)
    driver.maximize_window()
    driver.get(url)
    time.sleep(2)
    return driver



def get_ids():
    headers = {'user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:41.0) Gecko/20100101 Firefox/41.0'}
    kh_apt = "https://catalog.api.2gis.ru/3.0/items?key=ruhmxf0953&q=Доставка+продуктов&region_id=110&fields=items.locale,items.flags,search_attributes,items.adm_div,items.region_id,items.segment_id,items.reviews,items.point,request_type,context_rubrics,items.links,items.name_ex,items.org,items.group,items.external_content,items.comment,items.ads.options,items.stat,items.description,items.geometry.centroid,items.geometry.selection,items.geometry.style,items.timezone_offset,items.context,items.address,items.is_paid,items.access,items.access_comment,items.capacity,items.schedule,items.floors,dym,ad,items.rubrics,items.routes,items.purpose,items.see_also,items.route_logo,items.has_goods,items.has_pinned_goods,items.has_realty,items.has_payments,items.is_promoted,search_type,filters,widgets&type=adm_div.city,adm_div.district,adm_div.division,adm_div.living_area,adm_div.place,adm_div.settlement,attraction,branch,building,crossroad,foreign_city,gate,parking,road,route,station,street,coordinates&page_size=12&page=1&locale=ru_UA&allow_deleted=true&viewpoint1=35.64823947025923,50.53573097266493&viewpoint2=36.913040493929294,49.383756063782286&stat[sid]=4b89ae28-d9b8-46ae-bad1-f90e0b71fdd3&stat[user]=f4107cd2-a26b-449f-9b3d-80439746ea53&shv=2020-05-12-02&r=2165599669"
    answer = requests.get(kh_apt, headers=headers)
    ans = json.loads(answer.content.decode('utf-8'))
    res = []
    for i in ans["result"]["items"]:
        res.append(i["id"].split("_")[0])
    return res


ids = get_ids()
driver = get_driver(url)
time.sleep(2)

for i in ids:
    new_url = "https://2gis.ua/kharkov/search/Доставка+продуктов/firm/%s"%(id)
    driver.close()
    driver = get_driver(url)
    time.sleep(2)


