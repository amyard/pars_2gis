#!/usr/bin/env python
# -*- coding: utf-8 -*-



import time
import os

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities





city = "kharkov"
category = {
    "csv": "Доставка продуктов", "pars": "Доставка%20продуктов"
}


delivery_product_m = "https://m.2gis.ua/%s/search/%s"%(city,category["pars"])

def get_driver_mobile():


    mobile_emulation = {
        "deviceMetrics": {"width": 360, "height": 640, "pixelRatio": 3.0},
        "userAgent": "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
    }

    options = Options()
    options.add_experimental_option("mobileEmulation", mobile_emulation)
    caps = DesiredCapabilities().CHROME
    driver = webdriver.Chrome(
        executable_path='%s/chromedriver'%(os.getcwd()),
        desired_capabilities=caps,
        chrome_options = options,
    )
    time.sleep(2)
    return driver

def create_csv_header():
    f = open('example.csv','a+')
    f.write("city,category,item_url,title,sub_title,phone, redirect_urls,address,\n")
    f.close()
create_csv_header()



driver = get_driver_mobile()
driver.get(delivery_product_m)
time.sleep(2)


########################################################################################################
##########                click to display all items
########################################################################################################

driver.execute_script("document.getElementsByClassName('searchResults__content')[0].click();")
time.sleep(2)



########################################################################################################
##########               scroll down - preloader - Worked
########################################################################################################

driver.execute_script(""" function scrollDown() {
                                if (document.getElementsByClassName('preloader__container')[0] != null) {
                                    document.getElementsByClassName('preloader__container')[0].scrollIntoView({block: 'center', behavior: 'smooth'})
                                    setTimeout(function(){ scrollDown() }, 500)
                                }
                            }
                            scrollDown();""")

time.sleep(10)
########################################################################################################
##########               get Amount of items
########################################################################################################

soup = BeautifulSoup(driver.page_source, 'html.parser')
lal = soup.find_all("article", {'class': 'minicardPlain__container'})
li = driver.find_elements_by_class_name("minicardPlain__container")
print(len(li))

res_list = []
for i in range(len(lal)):
    print(i)
    res_1 = driver.find_elements_by_class_name("minicardPlain__title")[i]
    res = res_1.find_elements_by_tag_name("a")[0].get_attribute('href')
    res_list.append(res)
    print(res)



#######################################################################################################
#########               open new urls
#######################################################################################################

for nmb, url in enumerate(res_list):
    print(nmb, url)


# for url in res_list:
#     driver.get(url)
#     time.sleep(3)
#
#     pr_title = driver.find_element_by_xpath("//h1[@class='firmCardHeader__title']").text
#     pr_sub_title = driver.find_element_by_xpath("//h2[@class='firmCardHeader__extension']").text
#
#     pr_phone = []
#     pr_urls = []
#     contacts = driver.find_elements_by_class_name("contacts__contact")
#     for contact in contacts:
#         if "tel" in contact.get_attribute('href'):
#             pr_phone.append(contact.get_attribute('href').decode().encode('utf-8'))
#             pr_phone_res = "   ".join(i for i in pr_phone)
#         elif "facebook" not in contact.get_attribute('href') or "insta" not in contact.get_attribute('href'):
#             pr_urls.append(contact.text.decode().encode('utf-8'))
#             pr_urls_res = "   ".join(i for i in pr_urls)
#
#     pr_address = driver.find_element_by_xpath("//address[@class='address__container']").text
#     pr_address_res = " ".join(i for i in pr_address.split()).replace(",", "")
#
#     print(pr_title)
#     print(pr_sub_title)
#     print(pr_phone_res)
#     print(pr_urls_res)
#     print(pr_address_res)
#     print(category["csv"])
#
#     # save data
#     f = open('example.csv', 'a+')
#     f.write("%s,%s,%s,%s,%s,%s,%s,%s,\n"%(city.encode('utf-8'),
#                                           category["csv"].decode('utf8').encode('utf-8'),
#                                           url.encode('utf-8'),
#                                           pr_title.encode('utf-8'),
#                                           pr_sub_title.encode('utf-8'),
#                                           pr_phone_res.encode('utf-8'),
#                                           pr_urls_res.encode('utf-8'),
#                                           pr_address_res.encode('utf-8')
#                                           ))
#     f.close()
#
#     print('--------------------------------------')







#######################################################################################################
#########               FINISH TEST
#######################################################################################################

# ur = "https://m.2gis.ua/kharkov/search/%D0%94%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D0%BE%D0%B2/firm/70000001032730496"
# driver.get(ur)
# time.sleep(1)
#
# pr_title = driver.find_element_by_xpath("//h1[@class='firmCardHeader__title']").text
# pr_sub_title = driver.find_element_by_xpath("//h2[@class='firmCardHeader__extension']").text
#
# pr_phone = []
# pr_urls = []
# contacts = driver.find_elements_by_class_name("contacts__contact")
# for contact in contacts:
#     if "tel" in contact.get_attribute('href'):
#         pr_phone.append(contact.get_attribute('href'))
#     elif "facebook" not in contact.get_attribute('href') or "insta" not in contact.get_attribute('href'):
#         pr_urls.append(contact.text)
#
# pr_address = driver.find_element_by_xpath("//address[@class='address__container']").text
#
#
# print(pr_title)
# print(pr_sub_title)
# print(pr_phone)
# print(pr_urls)
# print(pr_address)