#!/usr/bin/env python
# -*- coding: utf-8 -*-



import time
import os

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException


doc_name = "cat_parse.csv"
replace_url = "https://m.2gis.ua/kharkov/search/"



def get_driver_mobile():
    mobile_emulation = {
        "deviceMetrics": {"width": 360, "height": 640, "pixelRatio": 3.0},
        "userAgent": "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
    }

    options = Options()
    options.add_experimental_option("mobileEmulation", mobile_emulation)
    caps = DesiredCapabilities().CHROME
    driver = webdriver.Chrome(
        executable_path='%s/chromedriver'%(os.getcwd()),
        desired_capabilities=caps,
        chrome_options = options,
    )
    time.sleep(2)
    return driver




# создаем шапку в файле для сохранения всех данных
def create_csv_header():
    f = open(doc_name,'a+')
    f.write("pars,rubric,category,sub-category\n")
    f.close()



def parse_data_clear(driver, cat):
    rubric = driver.find_element_by_xpath("//div[@class='rubricator__title']").text

    all_data = driver.find_elements_by_class_name("metarubrics__item")

    for item in all_data:
        href = item.get_attribute('href')
        if replace_url in href:
            href = href.replace(replace_url, "")
            sub_c = item.find_elements_by_class_name("metarubrics__title")[0].text
            sub_c = sub_c.replace(",", "- ")

            f = open(doc_name, 'a+')
            f.write("%s,%s,%s,%s,\n"%(
                href.encode('utf-8'),
                rubric.replace(",", " -").encode('utf-8'),
                cat.replace(",", " -").encode('utf-8').replace(",", "-") if cat else sub_c.encode('utf-8'),
                sub_c.encode('utf-8')
            ))
            f.close()     



def parse_data_dirty(driver, cat):
    rubric = driver.find_element_by_xpath("//div[@class='rubricator__title']").text

    all_data = driver.find_elements_by_class_name("rubricList__rubricItem")
    for item in all_data:
        href = item.get_attribute('href')
        href = href.replace(replace_url, "")
        sub_c = item.find_elements_by_class_name("rubricList__itemName")[0].text
        sub_c = sub_c.replace(",", "- ")


        f = open(doc_name, 'a+')
        f.write("%s,%s,%s,%s,\n"%(
            href.encode('utf-8'),
            rubric.replace(",", " -").encode('utf-8'),
            cat.replace(",", " -") if cat else sub_c.encode('utf-8'),
            sub_c.encode('utf-8')
        ))
        f.close()


def main():

    # create csv
    create_csv_header()

    ans_list = []

    # start parsing
    driver = get_driver_mobile()
    
    # clear
    urls = [
        "https://m.2gis.ua/kharkov/rubrics/110539",   # pars in cat_three_links
        "https://m.2gis.ua/kharkov/rubrics/110541",   # pars in cat_three_links
        "https://m.2gis.ua/kharkov/rubrics/110543",   # pars in cat_three_links
        "https://m.2gis.ua/kharkov/rubrics/110545",   # remove культура и организация праздников
        "https://m.2gis.ua/kharkov/rubrics/110549",   # remove медицина
        "https://m.2gis.ua/kharkov/rubrics/110552",   # remove автотранспорт
        "https://m.2gis.ua/kharkov/rubrics/110565",
        "https://m.2gis.ua/kharkov/rubrics/110574",
        "https://m.2gis.ua/kharkov/rubrics/110582",   # Напитки
        "https://m.2gis.ua/kharkov/rubrics/110585",   # Спорткомплексы
        "https://m.2gis.ua/kharkov/rubrics/110588",   # Все кроме одного
        "https://m.2gis.ua/kharkov/rubrics/110598",   # 4 штуки        
    ]
    for url in urls:
        driver.get(url)
        parse_data_clear(driver, "")
        time.sleep(0.5)


    # dirty
    urls = [
        {"url":"https://m.2gis.ua/kharkov/rubrics/111546","name":""}, 
        {"url":"https://m.2gis.ua/kharkov/rubrics/111548","name":""},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110544","name":""},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110548","name":""},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110546","name":"Развлечения"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110547","name":"Развлечения"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110550","name":"Медицина"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110550","name":"Медицина"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110554","name":"Автотовары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110555","name":"Автотовары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110557","name":"Товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110558","name":"Товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110559","name":"Товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110550","name":"Товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110561","name":"Товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110562","name":"Товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110563","name":"Товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110564","name":"Товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110573","name":""},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110575","name":""},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110581","name":"Спецмагазины"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110577","name":"Спецмагазины"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110578","name":"Спецмагазины"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110579","name":"Спецмагазины"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110580","name":"Спецмагазины"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110641","name":"Спецмагазины"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110642","name":"Спецмагазины"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110583","name":"Продукты"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110584","name":""},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110586","name":"Спорт"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/111550","name":""},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110644","name":"Образование"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110643","name":"Образование"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110589","name":"Образование"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110590","name":"Образование"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110591","name":"Образование"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110594","name":"Власть"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110595","name":"Власть"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110596","name":"Власть"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/111553","name":""},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110599","name":"Ремонт - стройка"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110600","name":"Ремонт - стройка"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110601","name":"Ремонт - стройка"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110571","name":"Ремонт - стройка"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110602","name":"Ремонт - стройка"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110604","name":"Пром товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110605","name":"Пром товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110606","name":"Пром товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110607","name":"Пром товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110608","name":"Пром товары"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110610","name":"В2В-услуги"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110611","name":"В2В-услуги"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110613","name":"В2В-услуги"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110614","name":"В2В-услуги"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110615","name":"В2В-услуги"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110616","name":"В2В-услуги"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110617","name":"В2В-услуги"},
        {"url":"https://m.2gis.ua/kharkov/rubrics/110618","name":"Другое"},
    ]
    for url in urls:
        driver.get(url["url"])
        parse_data_dirty(driver, url["name"])
        time.sleep(0.5)



if __name__ == '__main__':
    main()